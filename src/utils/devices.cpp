/*
    SPDX-FileCopyrightText: 2017 Nicolas Carion
    SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "devices.hpp"

#include <QStorageInfo>
#include <solid/block.h>
#include <solid/device.h>
#include <solid/storageaccess.h>
#include <solid/storagedrive.h>
#include <solid/storagevolume.h>

bool isOnRemovableDevice(const QUrl &file)
{
    return isOnRemovableDevice(file.path());
}

bool isOnRemovableDevice(const QString &path)
{
    QString mountPath = QStorageInfo(path).rootPath();

    // We list volumes to find the one with matching mount path

    for (const auto &device : Solid::Device::allDevices()) {
        if (device.is<Solid::StorageAccess>()) {
            auto interface = device.as<Solid::StorageAccess>();
            if (interface->filePath() == mountPath) {
                auto parentDevice = device.parent();

                // try to cope with encrypted devices
                if (!parentDevice.isValid() || !parentDevice.is<Solid::StorageDrive>()) {
                    if (device.is<Solid::StorageVolume>()) {
                        auto volume_interface = device.as<Solid::StorageVolume>();
                        auto encryptedDevice = volume_interface->encryptedContainer();
                        if (encryptedDevice.isValid()) {
                            parentDevice = encryptedDevice.parent();
                        }
                    }
                }

                if (parentDevice.isValid() && parentDevice.is<Solid::StorageDrive>()) {
                    auto parent_interface = parentDevice.as<Solid::StorageDrive>();
                    return parent_interface->isRemovable();
                }
            }
        }
    }
    // not found, defaulting to false
    return false;
}
